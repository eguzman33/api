//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res){
  //res.sendfile(path.join(__dirname,'index.html'));
  res.sendfile(path.join(__dirname,'cutsomerfake.json'));
});

app.get('/clientes/:idcliente',function(req,res){
  //res.sendfile(path.join(__dirname,'index.html'));
  res.send('aqui tiene el cliente numero:'+ req.params.idcliente);
});

app.post('/',function(req,res){
  res.send('Post recibido')
});

app.put('/',function(req,res){
  res.send('put recibido')
});

app.delete('/',function(req,res){
  res.send('Su petición a sido borrada')
});
